# presentation-git

## Voir cette présentation en ligne

https://bobmaerten.eu/slides/presentation-git/index.html#1

## Visualiser cette présentation en local

1. Clonez le dépot

    git clone https://gitlab.com/bobmaerten/presentation-git.git

2. Lancez votre navigateur préféré

    sensible-brower presentation-git/index.html
